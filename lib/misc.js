/**
 * Created by kilone on 04.12.13.
 */
var http = require('http');

module.exports.plainNativeHttpRequest = plainNativeHttpRequest;
function plainNativeHttpRequest ( config, deviantartId, cb ) {
    var url = deviantartId + '.' + config.url,
        port = config.port,
        path = config.path,
        ua = config.useragent;

    var options = {
        host: url,
        port: port,
        path: path,
        headers: {
            'User-Agent': ua
        }
    };

    console.log( options );



    callback = function( response ) {
        var str = '';

        response.setEncoding( 'binary' );

        //another chunk of data has been recieved, so append it to `str`
        response.on( 'data', function ( chunk ) {
            str += chunk;
        });

        //the whole response has been recieved, so we just print it out here
        response.on( 'end', function () {
            cb( response.statusCode, str );
        });
    }

    http.request( options, callback ).end();
}

module.exports.readJsonFile = readJsonFile;
function readJsonFile ( path ) {
    try {
        return require( path );
    }
    catch( err ) {
        console.log( err );
        return null;
    }
}

module.exports.downloadFile = downloadFile;
function downloadFile( config, deviantartId, url, cb ) {
    var urlObj = require('url'),
        fs = require('fs'),
        filename = urlObj.parse( url).path.split( '/' )[ ( urlObj.parse( url).path.split( '/').length -1 ) ];

    var file = fs.createWriteStream( config.downloadPath + deviantartId +'/' + filename );
    var request = http.get( url, function( response ) {
        response.pipe( file );
        file.on( 'finish', function( err ) {
            file.close();
            cb( err );
        });
    });
}