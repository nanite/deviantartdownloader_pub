var misc = require('./misc'),
    jsdom = require('jsdom'),
    _ = require('underscore'),
    async = require('async'),
    q,
    config;


module.exports.organiseModes = organiseModes;
function organiseModes( configArg ) {
    config = configArg;

    q = async.queue( function ( task, cb ) {
        task( cb );
    }, config.simultanDownloads );

    // assign a callback
    q.drain = function() {
        console.log( 'Queue is finished' );
    };

    if( config.args.username && !config.args.gallery ) {
        downloadGalleries( config.args.username );
    } else if( config.args.gallery ) {
        downloadGalleries( config.args.username, '/gallery/' + config.args.gallery, true );
    } else if( config.args.group ) {
        downloadGalleries( config.args.group );
    }
}

function downloadGalleries ( deviantartId, path, galleryMode ) {
    var mkdirp = require('mkdirp');

    // if new path from recursive exists
    if( path )
        config.web.path = path;

    if( !path || galleryMode ) {
        mkdirp( config.downloadPath + deviantartId, function( err ) {
            if( !err ) {
                misc.plainNativeHttpRequest( config.web, deviantartId, function( statusCode, content ) {
                    if( statusCode !== 200 ) {
                        console.log( "Error: Http-Statuscode: " + statusCode );
                    } else {
                        scanImagesAndDownloadPage( content, deviantartId );
                        getNextPage( content, deviantartId );
                    }
                });
            } else {
                console.log( err );
            }
        });
    }else {
        misc.plainNativeHttpRequest( config.web, deviantartId, function( statusCode, content ) {
            if( statusCode !== 200 ) {
                console.log( "Error: Http-Statuscode: " + statusCode );
            } else {
                scanImagesAndDownloadPage( content, deviantartId );
                getNextPage( content, deviantartId );
            }
        });
    }
}

function scanImagesAndDownloadPage( body, deviantartId ) {
    /*var vhtml = $.parseHTML( content)/*,
        pictureLinks = $( vhtml ).find( '.zones-container').find( '.thumb' );*/

    jsdom.env(
        body,
        ['http://code.jquery.com/jquery-1.9.1.min.js'], function(err, window) {
            var $ =  window.jQuery,
                pictureLinks = $( 'body' ).find( '.zones-container').find( '.thumb' );

            _.each( pictureLinks, function( picture ) {
                // check is link existing
                var imgPath = $( picture).attr( 'data-super-img' );

                if( imgPath )
                    q.push( function ( subCba ) {

                        misc.downloadFile( config, deviantartId, imgPath, function( err ) {
                            if( err )
                                console.log( err );

                            subCba();
                        });
                    });
            });
        });
}

function getNextPage( body, deviantartId ) {
    jsdom.env(
        body,
        ['http://code.jquery.com/jquery-1.9.1.min.js'], function(err, window) {
            var $ =  window.jQuery,
                nextLink = $( 'body' ).find( '.zones-container').find( '#gmi-GZone').find( '.next').find( '.away' );

            if( $( nextLink ).attr('id') === 'gmi-GPageButton' )
                downloadGalleries( deviantartId, nextLink.attr( 'href' ) );
        });
}
