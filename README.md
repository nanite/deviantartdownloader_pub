#README
This is a tool to download user galleries from deviantart

##Options
```
-u username : Download all galleries from this user
-g galleriyHash/galleryName : Download this gallery
```
##example:
```
node download.js --username USERNAME
node download.js --gallery ID_NUMBER/GALLERY_NAME --username USERNAME
```

For more options please check the "etc/config.json" file