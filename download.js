var program = require('commander'),
    downloader = require('./lib/downloader'),
    config = require('./etc/config.json'),
    package = require('./package.json');


program
    .version( package.version )
    .option( '-u, --username <username>', 'Download all images from this user' )
    .option( '-g, --gallery <gallery>', 'Download all images from this gallery' )
    //.option( '-G, --group <group>', 'Download all from this group' )
    .parse( process.argv );

if( program.username || ( program.username && program.gallery ) ) {
    config.args = program;
    downloader.organiseModes( config );
} else {
    var filename = process.argv[ 1 ].split( '/' );
    console.log( 'please type:\n\t' + process.argv[ 0 ] + ' ' + filename[ ( filename.length - 1 ) ] + ' --help' );
}
